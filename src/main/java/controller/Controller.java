package controller;

import model.FileRow;
import utilities.Utilities;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Controller {
    private String inputFile;
    private String outputFile;
    private List<FileRow> list = new ArrayList<>();

    public Controller(String inputFile, String outputFile) {
        this.inputFile = inputFile;
        this.outputFile = outputFile;
    }

    //Reads text file and stores rows to list
    public void parseFile() throws Exception {
        System.out.println("Parsing input file");
        try {
            File file = new File(inputFile);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
               list.add(new FileRow(line));
            }
            fileReader.close();
        } catch (IOException e) {
            throw new Exception("Input file not found. Exiting");
        }
    }

    public void sortTo() {
        System.out.println("Sorting file");
        Collections.sort(list);
    }

    public void writeOutput() throws Exception {
        System.out.println("Writing to output file");
        PrintWriter writer = new PrintWriter(outputFile, "UTF-8");
        for (FileRow row : list) {
            writer.println(row.getRow());
        }
        writer.close();
    }

    public List<FileRow> getList() {
        return this.list;
    }
}
