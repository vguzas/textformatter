package utilities;

public class Utilities {

    // If possible return as double
    public static Double getAsDouble(String str) {
        try {
            return Double.parseDouble(str);
        } catch (Exception e) {
            return null;
        }
    }
}
