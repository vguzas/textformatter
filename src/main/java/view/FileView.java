package view;

import model.FileRow;

import java.util.ArrayList;
import java.util.List;

public class FileView {
    private List<FileRow> list = new ArrayList<>();

    public FileView(List<FileRow> list) {
        this.list = list;
    }

    public void printToConsole() {
        for(FileRow row : list) {
            System.out.println(row.getRow());
        }
    }
}
