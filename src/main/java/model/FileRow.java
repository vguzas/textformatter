package model;

import utilities.Utilities;

import java.util.Comparator;

public class FileRow implements Comparable<FileRow> {
    private String row;
    private String delimiter = " ";

    public FileRow(String row) {
        this.row = row;
    }
    public FileRow(String row, String delimiter) {
        this.row = row;
        this.delimiter = delimiter;
    }

    public String getRow() {
        return this.row;
    }

    public String getDelimiter() {
        return this.delimiter;
    }

    public String[] getColumns() {
        return this.row.split(this.delimiter);
    }

    @Override
    public int compareTo(FileRow o) {
        String[] c1 = this.getColumns();
        String[] c2 = o.getColumns();

        // Loop through column values
        for (int i = 0; i < c1.length; i++) {
            // If second column has less elements then its' considered lower
            if (c2.length - 1 >= i) {
                // Get both columns as Doubles

                Double d1 = Utilities.getAsDouble(c1[i]);
                Double d2 = Utilities.getAsDouble(c2[i]);

                // Checking if number
                if (d1 != null || d2 != null) {
                    // If first element is a number and the second is a String
                    if (d1 != null && d2 == null) {
                        return -1;
                    } else if (d1 == null && d2 != null) {
                        // If second element is a number and the first is a String
                        return 1;
                    } else {
                        // Both elements are numbers
                        if (d1.compareTo(d2) != 0)
                            return d1.compareTo(d2);
                    }
                }
                //Comparing String values
                else if (c1[i].compareTo(c2[i]) != 0) {
                    return c1[i].compareTo(c2[i]);
                }
            } else {
                return -1;
            }
        }
        return 0;
    }
}
