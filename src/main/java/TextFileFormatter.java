import controller.Controller;
import view.FileView;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class TextFileFormatter {
    public static final String INPUT_FILE = "input.txt";
    public static final String OUTPUT_FILE = "output.txt";


    public static void main(String[] args) throws Exception {
        // Create a controller for file formatting
        Controller contr = new Controller(INPUT_FILE, OUTPUT_FILE);
        // Load file content into a list
        contr.parseFile();

        // Create a view to see input content
        FileView view = new FileView(contr.getList());
        System.out.println("INPUT -------------------------------");
        view.printToConsole();

        // Sort the file oontent
        contr.sortTo();

        // Print output file via view
        System.out.println("OUTPUT ------------------------------");
        view.printToConsole();

        // Write to output file
        contr.writeOutput();
    }
}
