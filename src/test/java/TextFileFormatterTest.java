import controller.Controller;
import model.FileRow;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import view.FileView;

import java.security.cert.CollectionCertStoreParameters;
import java.util.Collections;
import java.util.List;

public class TextFileFormatterTest {

    private static final String EXPECTED_OUTPUT = "-22 -3 4\n" +
            "-22 11 abc\n" +
            "-22 1234234 asdfasf asdgas\n" +
            "-2.2 2 3 4 329 2\n" +
            "-1.1\n" +
            "2.2 12345q 69 -afg\n" +
            "2.2 12345q 69 -asdf\n" +
            "\n" +
            "\n" +
            "qqqq 1.1";

    @Test
    public void testFileConversion() throws Exception {
        Controller contr = new Controller("input.txt", "output.txt");
        contr.parseFile();
        contr.sortTo();

        Assertions.assertEquals(EXPECTED_OUTPUT, getResult(contr.getList()));
    }

    private String getResult(List<FileRow> list) {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i < list.size(); i++) {
            sb.append(list.get(i).getRow());
            if (list.size() - 1 > i)
                sb.append("\n");
        }
        return sb.toString();
    }

}
